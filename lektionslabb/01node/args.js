const myArgs = process.argv.slice(2);

switch (myArgs[0]) { 
  case 'commit':
    console.log(`Create commit: ${myArgs[1]}`);
    break;
  case 'add':
    console.log('Add code');
    break;
  default:
    console.log(`Unknown argument: ${myArgs[0]}`);
}