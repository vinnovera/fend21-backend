import express from 'express';
const port = 3000;
const app = express();

app.set('views', './views');
app.set('view engine', 'ejs');

app.use(express.static('./public'));

const faq = [
  { id: 0, name: 'Contact', body: 'You can contact us' },
  { id: 1, name: 'Delivery time', body: '3-5 days' },
  { id: 2, name: 'Postage cost', body: '15 spänn' },
];

app.get('/about', (req, res) => { 
  res.render('about', {
    title: 'About',
    description: 'This is the about page',
    faq: faq,
    header: 'About page'
  });
});

app.get('/about/:faqid', (req, res) => {
  const usefaq = faq.find(item => item.id == req.params.faqid);
  if (!usefaq) { 
    res.status(404).send('Not found');
  }
  res.render('faq', {
    name: usefaq.name,
    body: usefaq.body
  });
});

app.listen(port, () => { 
  console.log(`Listening at ${port}`);
});
