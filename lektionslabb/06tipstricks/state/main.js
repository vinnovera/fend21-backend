const postForm = document.getElementById('post');
const postList = document.getElementById('postList');
const headlineInput = document.getElementById('headline');
const contentInput = document.getElementById('content');
const tagsInput = document.getElementById('tags');

const posts = [
  {
    headline: 'header 1',
    content: 'body 1',
    tags: ['test']
  },
  {
    headline: 'header 2',
    content: 'body 2',
    tags: ['test', 'best']
  }
]
  
let isEditing = false;

postForm.addEventListener('submit', (e) => {
  e.preventDefault();

  if (isEditing === false) {
    posts.push({
      headline: headlineInput.value,
      content: contentInput.value,
      tags: tagsInput.value.split(',')
    });
  } else {
    posts[isEditing] = {
      headline: headlineInput.value,
      content: contentInput.value,
      tags: tagsInput.value.split(',')
    }
  }

  postForm.reset();
  drawPostList(posts);
});

postForm.addEventListener('reset', (e) => {
  isEditing = false;
});

const editItem = (e) => { 
  const editItem = posts[parseInt(e.target.dataset.postid)];

  isEditing = e.target.dataset.postid;
  headlineInput.value = editItem.headline;
  contentInput.value = editItem.content;
  tagsInput.value = editItem.tags.join(',');
}

const renderTags = (tags) => tags.map(tag => `<li>${tag}</li>`).join('');

const drawPostList = (posts) => { 
  const postHTMLString = posts.map((post, i) => `
    <li>
      <h3>${post.headline}</h3>
      <p>${post.content}</p>
      <ul>${renderTags(post.tags)}</ul>
      <button data-postid="${i}">Edit</button>
    </li>`).join('');
  postList.innerHTML = postHTMLString;
  
  const buttons = document.querySelectorAll('li > button');
  buttons.forEach(btn => btn.addEventListener('click', editItem))
}

drawPostList(posts);
