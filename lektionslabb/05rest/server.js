import express from 'express';
const port = 3000;
const app = express();

app.use(express.static('public'));

app.get('/api/hello', (req, res) => {
  res.json({
    hello: 'world'
  });
});


app.listen(port, () => console.log(`Listning here: ${port}`));
