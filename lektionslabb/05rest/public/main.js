const readHello = async () => { 
  const res = await fetch('/api/hello');
  const data = await res.json();
  console.log(data);
}

readHello();
