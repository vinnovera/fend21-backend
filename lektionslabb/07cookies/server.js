import express from 'express';
import session from 'express-session';
import { MongoClient } from 'mongodb';
const port = 3000;
const app = express();

const client = new MongoClient('mongodb://127.0.0.1:27017');
await client.connect();
const db = client.db('loginproject');
const usersCollection = db.collection('users');

app.use(express.static('public'));

app.use(session({
  resave: false, // don't save session if unmodified
  saveUninitialized: false, // don't create session until something stored
  secret: 'shhhh, very secret',
}));

app.use(express.json());

app.post('/login', async (req, res) => {
  const user = await usersCollection.findOne({
    user: req.body.loginName,
    pass: req.body.loginPass
  });
  if (user) {
    req.session.user = user;
    res.json({
      user: user.user
    });
  } else { 
    res.status(401).send('Unauthorized');
  }
});

app.get('/user', (req, res) => {
  res.json({ user: req.session.user });
});

app.listen(port, () => console.log(`Listen on ${port}`));
