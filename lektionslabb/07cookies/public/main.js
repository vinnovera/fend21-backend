const loginForm = document.getElementById('login');
const loginName = document.getElementById('loginname');
const loginPass = document.getElementById('loginpass');

loginForm.addEventListener('submit', async (e) => { 
  e.preventDefault();

  const res = await fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      loginName: loginName.value,
      loginPass: loginPass.value
    })
  });
  const data = await res.json();
  console.log(data);
});

const getUser = async () => { 
  const res = await fetch('/user');
  const user = await res.json();

  console.log(user);
}

getUser();
