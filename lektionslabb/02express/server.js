import express from 'express';
const port = 3000;
const app = express();

app.use(express.static('public'));

app.use(express.urlencoded());

app.get('/hello', (req, res) => {
  console.log(req.query);
  res.json({
    message: 'Hello world'
  });
});

app.post('/message', (req, res) => { 
  console.log(req.body);
  res.json({
    message: req.body.message,
    sender: req.body.sender
  });
});

app.listen(port, () => {
  console.log(`Express listening on ${port}`);
});
