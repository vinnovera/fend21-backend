import readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('What is your name?', (name) => { 
  console.log(`Name is: ${name}`);
  rl.close();
});
