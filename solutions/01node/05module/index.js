import prompt from 'prompt';
import { question, getResponse } from './commands.js';

prompt.start();

const answer = await prompt.get(question);

getResponse(answer);
