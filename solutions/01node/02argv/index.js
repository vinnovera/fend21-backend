const myArgs = process.argv.slice(2);

switch (myArgs[0]) { 
  case 'greet':
    console.log(`Hello ${myArgs[1]}`);
    break;
  case 'farewell':
    console.log(`Goodbye ${myArgs[1]}`);
    break;
  default:
    console.log('Either "greet" or "farewell"');
    break;
}
