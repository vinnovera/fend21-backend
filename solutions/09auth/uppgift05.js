import express from 'express';
import session from 'express-session';
import { MongoClient, ObjectId } from 'mongodb';
import bcrypt from 'bcrypt';

const port = 3000;
const app = express();
const saltRounds = 10;

const client = new MongoClient('mongodb://127.0.0.1:27017');
await client.connect();
const db = client.db('loginproject');
const usersCollection = db.collection('users');

app.use(express.static('public'));

app.use(express.json());

app.use(session({
  resave: false, // don't save session if unmodified
  saveUninitialized: false, // don't create session until something stored
  secret: 'shhhh, very secret',
  cookie: {
    maxAge: 5 * 60 * 1000 // 5 minutes
  }
}));

const restrict = (req, res, next) => {
  if (req.session.user) {
    next();
  } else {
    res.status(401).send({ error: 'Unauthorized' });
  }
}

app.post('/api/login', async (req, res) => {
  const user = await usersCollection.findOne({ user: req.body.user });
  const passMatches = await bcrypt.compare(req.body.pass, user.pass);
  if (user && passMatches) {
    req.session.user = user.user;
    
    res.json({
      user: user.user
    });
  } else { 
    res.status(401).json({ error: 'Unauthorized' });
  }
});

app.get('/api/loggedin', (req, res) => {
  if (req.session.user) {
    res.json({
      user: req.session.user
    });
  } else { 
    res.status(401).json({ error: 'Unauthorized' });
  }
});

app.post('/api/logout', (req, res) => {
  req.session.destroy(() => {
    res.json({
      loggedin: false
    });
  });
});

app.post('/api/register', async (req, res) => {
  const hash = await bcrypt.hash(req.body.pass, saltRounds);

  await usersCollection.insertOne({
    user: req.body.user,
    pass: hash
  });

  res.json({
    success: true,
    user: req.body.user
  });
});

app.get('/api/secretdata', restrict, (req, res) => {
  res.json({
    secret: 'Javascript is fun!'
  });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
