const loginForm = document.getElementById('login');
const logoutForm = document.getElementById('logout');
const registerForm = document.getElementById('register');
const formUser = document.getElementById('user');
const formPass = document.getElementById('pass');
const regUser = document.getElementById('reguser');
const regPass = document.getElementById('regpass');
const welcomeElem = document.getElementById('welcome');
const secretBtn = document.getElementById('secretbtn');
const secretOutput = document.getElementById('secretoutput');

loginForm.addEventListener('submit', async (e) => {
  e.preventDefault();

  await fetch('/api/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user: formUser.value,
      pass: formPass.value
    })
  });
  
  location.reload();
});

logoutForm.addEventListener('submit', async (e) => {
  e.preventDefault();

  await fetch('/api/logout', { method: 'POST' });
  
  location.reload();
});

registerForm.addEventListener('submit', async (e) => {
  e.preventDefault();

  const res = await fetch('/api/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user: regUser.value,
      pass: regPass.value
    })
  });
  const data = await res.json();
  
  welcomeElem.innerText = `Tack för att du registrerat dig, ${data.user}!`;
});

const checkLoggedin = async () => { 
  const res = await fetch('/api/loggedin');
  const data = await res.json();

  if (data.user) {
    loginForm.style.display = 'none';
    welcomeElem.innerText = `Välkommen ${data.user}!`;
  } else {
    logoutForm.style.display = 'none';
  }
}

checkLoggedin();

secretBtn.addEventListener('click', async () => {
  const res = await fetch('/api/secretdata');
  
  if (res.status === 401) {
    secretOutput.innerText = 'That is a secret!';
  } else { 
    const data = await res.json();
    secretOutput.innerText = data.secret;
  }
});
