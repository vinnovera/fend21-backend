const loginForm = document.getElementById('login');
const formUser = document.getElementById('user');
const formPass = document.getElementById('pass');
const welcomeElem = document.getElementById('welcome');

loginForm.addEventListener('submit', async (e) => {
  e.preventDefault();

  const res = await fetch('/api/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user: formUser.value,
      pass: formPass.value
    })
  });
  const data = await res.json();
  
  if (data.user) { 
    loginForm.style.display = 'none';
    welcomeElem.innerText = `Välkommen ${data.user}!`;
  }
});