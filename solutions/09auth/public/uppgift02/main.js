const loginForm = document.getElementById('login');
const logoutForm = document.getElementById('logout');
const formUser = document.getElementById('user');
const formPass = document.getElementById('pass');
const welcomeElem = document.getElementById('welcome');

loginForm.addEventListener('submit', async (e) => {
  e.preventDefault();

  await fetch('/api/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user: formUser.value,
      pass: formPass.value
    })
  });
  
  location.reload();
});

logoutForm.addEventListener('submit', async (e) => {
  e.preventDefault();

  await fetch('/api/logout', { method: 'POST' });
  
  location.reload();
});

const checkLoggedin = async () => { 
  const res = await fetch('/api/loggedin');
  const data = await res.json();

  if (data.user) {
    loginForm.style.display = 'none';
    welcomeElem.innerText = `Välkommen ${data.user}!`;
  } else {
    logoutForm.style.display = 'none';
  }
}

checkLoggedin();
