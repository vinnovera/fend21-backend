import express from 'express';
import session from 'express-session';
const port = 3000;
const app = express();

app.use(express.static('public'));

app.use(express.json());

app.use(session({
  resave: false, // don't save session if unmodified
  saveUninitialized: false, // don't create session until something stored
  secret: 'shhhh, very secret',
  cookie: {
    maxAge: 5 * 60 * 1000 // 5 minutes
  }
}));

app.post('/api/login', (req, res) => {
  if (req.body.user === 'admin' && req.body.pass === '12345') {
    req.session.user = 'admin';
    
    res.json({
      user: 'admin'
    });
  } else { 
    res.status(401).json({ error: 'Unauthorized' });
  }
});

app.listen(port, () => console.log(`Listening on port ${port}`));
