import express from 'express';
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.json({
    message: 'Welcome'
  });
});

app.get('/hello', (req, res) => {
  res.json({
    message: 'Hello World'
  });
});

app.get('/echo', (req, res) => {
  res.json({
    message: req.query.message
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});
