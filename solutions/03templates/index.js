import express from 'express';
const app = express();
const port = 3000;

// Var skall Express leta efter views
app.set('views', './views');

// Vilken "template engine" skall användas
app.set('view engine', 'ejs');

// Statiska filer som t.ex. stylesheets behöver fortfarande kunna läsas
app.use(express.static('public'));

// Dummydata, i praktiken skulle detta antagligen komma från en databas
const users = [
  { id: 0, name: 'Peter', email: 'peter@mail.com' },
  { id: 1, name: 'Mary', email: 'mary@mail.com' },
  { id: 2, name: 'Jane', email: 'jane@mail.com' }
];

// Routes
app.get('/', (req, res) => {
  res.render('index', {
    title: 'User list',
    message: 'This is a list of users',
    users: users
  });
});

app.get('/user/:userid', (req, res) => {
  const user = users.find((user) => user.id == req.params.userid);
  if (!user) { 
    res.status(404).send('Not found');
  }
  res.render('user', {
    user: user
  });
});

// Börja lyssna efter trafik
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});
