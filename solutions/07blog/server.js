import express from 'express';
import { MongoClient, ObjectId } from 'mongodb';
const port = 3000;
const app = express();

app.use(express.json());

// Mer kod här sen
const client = new MongoClient('mongodb://127.0.0.1:27017');
await client.connect();
const db = client.db('blog');
const entriesCollection = db.collection('entries');

app.get('/entries', async (req, res) => {
  const limit = parseInt(req.query.limit) || 0; // If there is no such parameter, use 0, dont forget to parse
  let entries = await entriesCollection.find({}).limit(limit).toArray();
  res.json(entries);
});

app.get('/entries/:id', async (req, res) => {
  const entry = await entriesCollection.findOne({ _id: ObjectId(req.params.id) });
  res.json(entry);
});

app.put('/entries/:id', async (req, res) => {
  let entry = await entriesCollection.findOne({ _id: ObjectId(req.params.id) });
  entry = {
    ...entry,
    ...req.body,
    date: new Date(req.body.date)
  };
  await entriesCollection.updateOne({ _id: ObjectId(req.params.id) }, { $set: entry });
  res.json({
    success: true,
    entry
  });
});

app.delete('/entries/:id', async (req, res) => {
  await entriesCollection.deleteOne({ _id: ObjectId(req.params.id) });
  res.status(204).send();
});

app.post('/entries', async (req, res) => {
  const entry = {
    ...req.body,
    date: new Date(req.body.date)
  };
  await entriesCollection.insertOne(entry);
  res.json({
    success: true,
    entry
  });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
