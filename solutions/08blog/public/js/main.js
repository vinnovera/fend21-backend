const postForm = document.getElementById('createpost');
const formTitle = document.getElementById('title');
const formContent = document.getElementById('content');
const formTags = document.getElementById('tags');
const postsList = document.getElementById('posts');

const FORM_MODES = {
  CREATE: 'create',
  EDIT: 'edit'
}
let formMode = FORM_MODES.CREATE;
let editPostItem = null;

let entries = [];

const tagsTemplate = (tags) => tags.map((tag) => `
  <li>${tag}</li>
`).join('');

const postTemplate = (entry) => `
  <li>
    <h3>${entry.title}</h3>
    <p>${entry.date}</p>
    <p>${entry.content}</p>
    <ul>
      ${tagsTemplate(entry.tags)}
    </ul>
    <button data-function="edit" data-postid="${entry._id}">Edit Post</button>
    <button data-function="delete" data-postid="${entry._id}">Remove Post</button>
  </li>
`;

const deletePost = async (e) => { 
  await fetch(`/api/entries/${e.target.dataset.postid}`, {
    method: 'DELETE'
  });

  readPosts();
} 

const editPost = async (e) => { 
  formMode = FORM_MODES.EDIT;
  editPostItem = entries.find(({ _id }) => _id === e.target.dataset.postid);
  
  formTitle.value = editPostItem.title;
  formContent.value = editPostItem.content;
  formTags.value = editPostItem.tags.join(',');
}

const addButtonListeners = () => { 
  const deleteBtns = document.querySelectorAll('[data-function="delete"]');
  deleteBtns.forEach(btn => btn.addEventListener('click', deletePost));

  const editBtns = document.querySelectorAll('[data-function="edit"]');
  editBtns.forEach(btn => btn.addEventListener('click', editPost));
}

const readPosts = async () => { 
  const res = await fetch('/api/entries');
  entries = await res.json();

  postsList.innerHTML = entries.map(postTemplate).join('');
  addButtonListeners();
}

postForm.addEventListener('reset', async (e) => {
  formMode = FORM_MODES.CREATE;
  editPostItem = null;
});

postForm.addEventListener('submit', async (e) => {
  e.preventDefault();
  
  const useUrl = formMode === FORM_MODES.CREATE ? '/api/entries' : `/api/entries/${editPostItem._id}`;
  const method = formMode === FORM_MODES.CREATE ? 'POST' : 'PUT';
  const date = formMode === FORM_MODES.CREATE ? new Date() : new Date(editPostItem.date);

  await fetch(useUrl, {
    method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      title: formTitle.value,
      content: formContent.value,
      date,
      tags: formTags.value.split(',')
    })
  });
  
  postForm.reset();
  readPosts();
});

// Start fetching
readPosts();